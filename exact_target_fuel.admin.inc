<?php
/**
 * @file
 * Holds the system settings form for the API.
 */

/**
 * Admin form callback for Client settings.
 */
function exact_target_fuel_admin_form() {
  $form = array();

  $form['exact_target_fuel_intro'] = array(
    '#markup' => t('Please enter your API credentials for the Exact Target SOAP/REST APIs.'),
  );

  $form['exact_target_fuel_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Id'),
    '#required' => TRUE,
    '#default_value' => variable_get('exact_target_fuel_client_id', ''),
  );

  $form['exact_target_fuel_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#required' => TRUE,
    '#default_value' => variable_get('exact_target_fuel_client_secret', ''),
  );

  $form['exact_target_fuel_list_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Subscriber List'),
    '#required' => TRUE,
    '#default_value' => variable_get('exact_target_fuel_list_id', ''),
  );

  return system_settings_form($form);
}
